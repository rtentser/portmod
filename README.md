# Portmod
[![pipeline](https://gitlab.com/portmod/portmod/badges/master/pipeline.svg)](https://gitlab.com/portmod/portmod/-/commits/master)
[![Build status](https://ci.appveyor.com/api/projects/status/73nlk92oj22jbyfj/branch/master?svg=true&passingText=Windows%20OK&failingText=windows%20failed)](https://ci.appveyor.com/project/portmod/portmod/branch/master)
[![coverage](https://gitlab.com/portmod/portmod/badges/master/coverage.svg)](https://gitlab.com/portmod/portmod/-/commits/master)
[![PyPI](https://img.shields.io/pypi/v/portmod)](https://pypi.org/project/portmod/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

A cross-platform cli package manager for mods. Based on Gentoo's [Portage](https://wiki.gentoo.org/wiki/Portage) package manager.

Currently the only supported game is OpenMW, via the [openmw-mods](https://gitlab.com/portmod/openmw-mods) repository.

See [the Wiki](https://gitlab.com/portmod/portmod/wikis/home) for details on [Installation](https://gitlab.com/portmod/portmod/wikis/Installation/Installation) and [Setup](https://gitlab.com/portmod/portmod/wikis/Setup).

## Features

- **Automatic Downloads (where possible)**: If direct links are available to mod archive files, portmod will fetch them automatically. As many mods have restrictive licenses and are distributed via sites which do not provide direct links, this is not always possible.
- **Automatic patching**: Portmod organizes mods into packages which contain both the base mod and any necessary patches, which are enabled based on the other mods you have installed.
- **Automatic Configuration**: Mod packages can declare optional features (called [Use Flags](https://gitlab.com/portmod/portmod/-/wikis/configuration/use-flags)), which can either be independently enabled/disabled (local flags), or enabled/disabled along with other packages which share the same feature (global flags).
- **Structure Awareness**: Portmod's package files contain information about the directory structure of the archives which explain precisely how the mod should be installed.
- **Automatic sorting**: The install order and the load order of plugin and fallback archive files are sorted automatically based on rules defined in the packages. These rules can be customized with [user rules](https://gitlab.com/portmod/portmod/-/wikis/configuration/user-sorting-rules).
- **Automatic updates**: When mod updates are released and new package files are created in the portmod repository, you can find and update all your installed mods using a single command. Due to portmod requiring more information than upstream sources usually provide, will be a delay after the upstream mod is updated while new package files are created.
- **Dependencies**: Portmod will automatically install dependencies for the mods you ask it to install.
- **Mod collections**: Portmod supports both metapackages (packages containing lists of other packages which are distributed in the repository), as well as custom [package sets](https://gitlab.com/portmod/portmod/-/wikis/configuration/sets) (easy to set-up package lists that exist in the user's configuration).

## Basic Usage

There are a number of executables installed with Portmod, however for the most part you only need to use the `omwmerge` executable.

Mods can be installed by passing the relevant atoms as command line arguments. E.g.:
`omwmerge omwllf`

You can search for mods using the `--search`/`-s` (searches name only) and `--searchdesc`/`-S` (searches name and description) options.

Specific versions of mods can be installed by including the version number: `omwmerge abandoned-flat-2.0`

Specified mods will be automatically be downloaded, configured and installed.

The `-c`/`--depclean` flag will remove the specified mods and all mods that depend on, or are dependencies of, the specified mods.

The `-C`/`--unmerge` flag will remove the specified mods, ignoring dependencies

You can view useful information about your current setup using `omwmerge --info`.

You can update all installed mods (including dependencies and dependency changes) using the command `omwmerge --update --deep --newuse @world` (or `omwmerge -uDN @world`)

After updates, you should clean unneeded dependencies using `omwmerge --depclean` (without any arguments). This can also be done during updates using the `--auto-depclean`/`-x` flag, however you should carefully examine the transaction list to make sure that it isn't removing anything you wanted to keep.

## Future Plans

This list is updated infrequently. See the [Issues](https://gitlab.com/portmod/portmod/-/issues) for up-to-date information.

- **Support for other game engines and multiple games**: Support for other engines is already feasible (portmod's implementation is fairly generic), but restricted by the lack of support for installing into a single data directory. Currently games must support per-mod data directories. There is also not yet a way to distinguish between mods installed for different game engines.
- **Mod packaging and submission**: Support for build files being distributed with mods and automatically submitted to a repository using a cli tool.

[![Packaging status](https://repology.org/badge/vertical-allrepos/portmod.svg)](https://repology.org/project/portmod/versions)
