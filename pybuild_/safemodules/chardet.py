# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""
A wrapper around chardet that provides just the detect function
"""

from chardet import detect  # noqa  # pylint: disable=unused-import
